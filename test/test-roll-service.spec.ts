
import assert = require('assert')

import moment = require('moment')

import { RollService } from '../src/service/roll-service'
import { HitterDao } from '../src/dao/hitter-dao'
import { PitcherDao } from '../src/dao/pitcher-dao'
import { Hitter } from '../src/dto/hitter'
import { RollInput } from '../src/dto/roll-input'
import { RollChart } from '../src/dto/roll-chart'
import { Pitcher } from '../src/dto/pitcher'
const fs = require('fs').promises



//@ts-ignore
describe('RollService', async (accounts) => {

    let service: RollService
    let hitterDao = new HitterDao()
    let pitcherDao = new PitcherDao()

    //@ts-ignore
    before('Before', async () => {
        service = new RollService(hitterDao, pitcherDao)
    })

    //@ts-ignore
    it("should get league averages", async () => {

        // //Act
        let result:RollInput = await service.getHitterLeagueAverages(2019)

        // //Assert
        assert.equal(result.hit, 32)
        assert.equal(result.k, 23)
        assert.equal(result.out, 45)

        assert.equal(result.hit + result.k + result.out, 100)


        assert.equal(result.bb, 31)
        assert.equal(result.singles, 43)
        assert.equal(result.doubles, 14)
        assert.equal(result.triples, 1)
        assert.equal(result.hr, 11)

        assert.equal(result.bb + result.singles + result.doubles + result.triples + result.hr, 100)

    })

    //@ts-ignore
    it("should get yelich", async () => {

        // //Act
        let left:RollInput = await service.getHitterYearRollInput("592885", 2019, "L")
        let right:RollInput = await service.getHitterYearRollInput("592885", 2019, "L")


        // //Assert
        assert.equal(left.hit, 41)
        assert.equal(left.k, 18)
        assert.equal(left.out, 41)

        assert.equal(left.hit + left.k + left.out, 100)

        assert.equal(left.bb, 41)
        assert.equal(left.singles, 30)
        assert.equal(left.doubles, 12)
        assert.equal(left.triples, 1)
        assert.equal(left.hr, 16)

        assert.equal(left.bb + left.singles + left.doubles + left.triples + left.hr, 100)




        assert.equal(right.hit, 41)
        assert.equal(right.k, 18)
        assert.equal(right.out, 41)

        assert.equal(right.hit + right.k + right.out, 100)

        assert.equal(right.bb, 41)
        assert.equal(right.singles, 30)
        assert.equal(right.doubles, 12)
        assert.equal(right.triples, 1)
        assert.equal(right.hr, 16)

        assert.equal(right.bb + right.singles + right.doubles + right.triples + right.hr, 100)



    })


    //@ts-ignore
    it("should get yates", async () => {

        // //Act
        let left:RollInput = await service.getPitcherYearRollInput("489446", 2019, "L")
        let right:RollInput = await service.getPitcherYearRollInput("489446", 2019, "R")


        // //Assert
        assert.equal(left.hit, 27)
        assert.equal(left.k, 41)
        assert.equal(left.out, 32)

        assert.equal(left.hit + left.k + left.out, 100)


        assert.equal(left.bb, 31)
        assert.equal(left.singles, 40)
        assert.equal(left.doubles, 23)
        assert.equal(left.triples, 0)
        assert.equal(left.hr, 6)

        assert.equal(left.bb + left.singles + left.doubles + left.triples + left.hr, 100)


        assert.equal(right.hit, 24)
        assert.equal(right.k, 42)
        assert.equal(right.out, 34)

        assert.equal(right.hit + right.k + right.out, 100)


        assert.equal(right.bb, 37)
        assert.equal(right.singles, 52)
        assert.equal(right.doubles, 11)
        assert.equal(right.triples, 0)
        assert.equal(right.hr, 0)

        assert.equal(right.bb + right.singles + right.doubles + right.triples + right.hr, 100)


    })


    //@ts-ignore
    it("should get yelich contact charts", async () => {

        
        let yelich:RollInput = await service.getHitterYearRollInput("592885", 2019, "L")

        let contactChart:RollChart = service.getContactRollChart(yelich)

        for (let i=0; i<18; i++) {
            assert.equal(contactChart.entries.get(i),"K")
        }

        for (let i=18; i < 59; i++) {
            assert.equal(contactChart.entries.get(i),"O")
        }
        for (let i=59; i < 100; i++) {
            assert.equal(contactChart.entries.get(i),"H")
        }


        let powerChart:RollChart = service.getPowerRollChart(yelich)

        for (let i=0; i<41; i++) {
            assert.equal(powerChart.entries.get(i),"BB")
        }

        for (let i=41; i<71; i++) {
            assert.equal(powerChart.entries.get(i),"1B")
        }

        for (let i=71; i<83; i++) {
            assert.equal(powerChart.entries.get(i),"2B")
        }

        for (let i=83; i<84; i++) {
            assert.equal(powerChart.entries.get(i),"3B")
        }

        for (let i=84; i<100; i++) {
            assert.equal(powerChart.entries.get(i),"HR")
        }


        //Check contact diff
        let leagueAverage:RollInput = await service.getHitterLeagueAverages(2019)
        let laRollChart:RollChart = service.getContactRollChart(leagueAverage)

        let contactDiff:RollChart = service.diffRollChart(laRollChart, contactChart)

        for (let i=18; i<23; i++) {
            assert.equal(contactDiff.entries.get(i),"O")
        }

        for (let i=59; i<68; i++) {
            assert.equal(contactDiff.entries.get(i),"H")
        }

        //Check contact diff
        let laPowerRollChart:RollChart = service.getPowerRollChart(leagueAverage)

        let powerDiff:RollChart = service.diffRollChart(laPowerRollChart, powerChart)

        for (let i=31; i<41; i++) {
            assert.equal(powerDiff.entries.get(i),"BB")
        }

        for (let i=71; i<74; i++) {
            assert.equal(powerDiff.entries.get(71),"2B")
        }

        assert.equal(powerDiff.entries.get(83),"3B")

        for (let i=84; i<89; i++) {
            assert.equal(powerDiff.entries.get(i),"HR")
        }


    })

    //@ts-ignore
    it("should sim 5000 yelich at-bats", async () => {


        let result:RollInput = await service.simSinglePlayer(592885, 2019, 5000)

        //Get the real year to compare to.
        await hitterDao.setYear(2019)
        let hitter:Hitter = hitterDao.read("592885")

        console.log(`           Target`)
        console.log(`K : ${result.k.toFixed(3)} - ${hitter.leftK.toFixed(3)}`)
        console.log(`O : ${result.out.toFixed(3)} - ${hitter.leftOut.toFixed(3)}`)
        console.log(`H : ${result.hit.toFixed(3)} - ${hitter.leftHit.toFixed(3)}`)
        console.log(`BB: ${result.bb.toFixed(3)} - ${hitter.leftBb.toFixed(3)}`)
        console.log(`1B: ${result.singles.toFixed(3)} - ${hitter.leftSingle.toFixed(3)}`)
        console.log(`2B: ${result.doubles.toFixed(3)} - ${hitter.leftDouble.toFixed(3)}`)
        console.log(`3B: ${result.triples.toFixed(3)} - ${hitter.leftTriple.toFixed(3)}`)
        console.log(`HR: ${result.hr.toFixed(3)} - ${hitter.leftHr.toFixed(3)}`)

    })

    //@ts-ignore
    it("should merge roll charts", async () => {

        let leagueAverage:RollInput = await service.getHitterLeagueAverages(2019)
        let laRollChart:RollChart = service.getContactRollChart(leagueAverage)


        let yelich:RollInput = await service.getHitterYearRollInput("592885", 2019, "L")
        let yelichContact:RollChart = service.getContactRollChart(yelich)
        let yelichDiff = service.diffRollChart(laRollChart, yelichContact)
        

        let yates:RollInput = await service.getPitcherYearRollInput("489446", 2019, "L")
        let yatesContact:RollChart = service.getContactRollChart(yates)
        let yatesDiff = service.diffRollChart(laRollChart, yatesContact)


        let merged:RollChart = service.applyChartDiffs(yelichDiff, yatesDiff, laRollChart)

        for (let i=18; i<23; i++) {
            assert.equal(merged.entries.get(i),"O")
        }

        
        //23-41 - K 
        for (let i=23; i<41; i++) {
            assert.equal(merged.entries.get(i),"K")
        }


        //59-67 - H 
        for (let i=59; i<68; i++) {
            assert.equal(merged.entries.get(i),"H")
        }

        //67-73 - O
        for (let i=68; i<73; i++) {
            assert.equal(merged.entries.get(i),"O")
        }

    })



    //@ts-ignore
    it("should sim matchups", async () => {

        let result:RollInput = await service.simMatchup("592885", "489446", 2019, 5000)

        //Get the real year to compare to.
        await hitterDao.setYear(2019)
        let hitter:Hitter = hitterDao.read("592885")


        console.log(`           Yelich`)
        console.log(`K : ${result.k.toFixed(3)} - ${hitter.leftK.toFixed(3)}`)
        console.log(`O : ${result.out.toFixed(3)} - ${hitter.leftOut.toFixed(3)}`)
        console.log(`H : ${result.hit.toFixed(3)} - ${hitter.leftHit.toFixed(3)}`)
        console.log(`BB: ${result.bb.toFixed(3)} - ${hitter.leftBb.toFixed(3)}`)
        console.log(`1B: ${result.singles.toFixed(3)} - ${hitter.leftSingle.toFixed(3)}`)
        console.log(`2B: ${result.doubles.toFixed(3)} - ${hitter.leftDouble.toFixed(3)}`)
        console.log(`3B: ${result.triples.toFixed(3)} - ${hitter.leftTriple.toFixed(3)}`)
        console.log(`HR: ${result.hr.toFixed(3)} - ${hitter.leftHr.toFixed(3)}`)

    })


    //@ts-ignore
    it("should sim yelich against every pitcher", async () => {

        let results:RollInput[] = []

        let pitchers:Pitcher[] = pitcherDao.list()

        for (let pitcher of pitchers) {
            results.push(await service.simMatchup("592885", pitcher.id, 2019, 1000))
        }

        let contactSum = results.map(result => result.hit).reduce((a, b) => a + b)
        let kSum = results.map(result => result.k).reduce((a, b) => a + b)
        let outSum = results.map(result => result.out).reduce((a, b) => a + b)

        let bbSum = results.map(result => result.bb).reduce((a, b) => a + b)
        let singleSum = results.map(result => result.singles).reduce((a, b) => a + b)
        let doubleSum = results.map(result => result.doubles).reduce((a, b) => a + b)
        let tripleSum = results.map(result => result.triples).reduce((a, b) => a + b)
        let hrSum = results.map(result => result.hr).reduce((a, b) => a + b)

        let contact = contactSum / results.length
        let k = kSum / results.length
        let out = outSum / results.length
         
        let bb = bbSum /results.length
        let single = singleSum / results.length
        let double = doubleSum / results.length
        let triple = tripleSum / results.length
        let hr = hrSum / results.length


        //Get the real year to compare to.
        await hitterDao.setYear(2019)
        let hitter:Hitter = hitterDao.read("592885")


        console.log(`           Yelich`)
        console.log(`K : ${k.toFixed(3)} - ${hitter.leftK.toFixed(3)}`)
        console.log(`O : ${out.toFixed(3)} - ${hitter.leftOut.toFixed(3)}`)
        console.log(`H : ${contact.toFixed(3)} - ${hitter.leftHit.toFixed(3)}`)
        console.log(`BB: ${bb.toFixed(3)} - ${hitter.leftBb.toFixed(3)}`)
        console.log(`1B: ${single.toFixed(3)} - ${hitter.leftSingle.toFixed(3)}`)
        console.log(`2B: ${double.toFixed(3)} - ${hitter.leftDouble.toFixed(3)}`)
        console.log(`3B: ${triple.toFixed(3)} - ${hitter.leftTriple.toFixed(3)}`)
        console.log(`HR: ${hr.toFixed(3)} - ${hitter.leftHr.toFixed(3)}`)

    })

    //@ts-ignore
    it("should save league averages", async () => {

        // //Act
        let leagueAverage:RollInput = await service.getHitterLeagueAverages(2019)

        let contact:RollChart = service.getContactRollChart(leagueAverage)
        let power:RollChart = service.getPowerRollChart(leagueAverage)

        let contactEntries = Array.from(contact.entries.entries())
        let powerEntries = Array.from(power.entries.entries())

        // console.log('Contact')
        // for (let entry of contactEntries) {
        //     console.log(`${entry[0]}:${entry[1]}`)
        // }

        // console.log('Power')
        // for (let entry of powerEntries) {
        //     console.log(`${entry[0]}:${entry[1]}`)
        // }


        await fs.writeFile(`../www/cards/2019-league.csv`, JSON.stringify({
            contact: contactEntries,
            power: powerEntries
        }))



    })


})
