
import assert = require('assert')

import moment = require('moment')
import { PitcherDao} from "../src/dao/pitcher-dao"
import { Pitcher } from '../src/dto/pitcher'


//@ts-ignore
describe('PitcherDao', async (accounts) => {

    let dao: PitcherDao


    //@ts-ignore
    before('Before', async () => {
        dao = new PitcherDao()
    })    

    //@ts-ignore
    it("should load 2019", async () => {

        await dao.setYear(2019)

        let players:Pitcher[] = dao.list()

        assert.equal(players.length, 311)

    })


})
