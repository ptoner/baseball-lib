
import assert = require('assert')
import moment = require('moment')

import { RawPitcherService } from '../src/service/raw-pitcher-service'
import { MlbApiService } from '../src/service/mlb-api-service'


//@ts-ignore
describe('RawPitcherService', async (accounts) => {

    let service: RawPitcherService

    //@ts-ignore
    before('Before', async () => {
        service = new RawPitcherService(new MlbApiService())
    })

    //@ts-ignore
    it("process pitchers", async () => {

        await service.processPitchers(2019)
        
    })


})
