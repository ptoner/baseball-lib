
import assert = require('assert')

import moment = require('moment')

import { RollService } from '../src/service/roll-service'
import { PitcherCardService } from '../src/service/pitcher-card-service'

import { HitterDao } from '../src/dao/hitter-dao'
import { PitcherDao } from '../src/dao/pitcher-dao'
import { Hitter } from '../src/dto/hitter'
import { RollInput } from '../src/dto/roll-input'
import { RollChart } from '../src/dto/roll-chart'
import { Pitcher } from '../src/dto/pitcher'
import { HitterCard } from '../src/dto/hitter-card'
import { PitcherCard } from '../src/dto/pitcher-card'
const fs = require('fs').promises



//@ts-ignore
describe('PitcherCardService', async (accounts) => {

    let service: PitcherCardService
    let rollService:RollService
    let hitterDao = new HitterDao()
    let pitcherDao = new PitcherDao()

    //@ts-ignore
    before('Before', async () => {
        rollService = new RollService(hitterDao, pitcherDao)
        service = new PitcherCardService(pitcherDao, rollService)
    })

    //@ts-ignore
    it("should get a list of cards ", async () => {
        let cards:PitcherCard[] = await service.list(2019)

        cards = cards.filter(card => card.team == "HOU" || card.team == "WSH")

        // assert.equal(cards.length, 523)

        //Check yates
        // let yates:PitcherCard = cards[4]

        // assert.equal(yates.contact[0].start, 23)
        // assert.equal(yates.contact[0].end, 41)
        // assert.equal(yates.contact[0].value, "K")

        // assert.equal(yates.contact[1].start, 68)
        // assert.equal(yates.contact[1].end, 73)
        // assert.equal(yates.contact[1].value, "O")


        // assert.equal(yates.power[0].start, 31)
        // assert.equal(yates.power[0].end, 33)
        // assert.equal(yates.power[0].value, "BB")

        // assert.equal(yates.power[1].start, 74)
        // assert.equal(yates.power[1].end, 78)
        // assert.equal(yates.power[1].value, "1B")

        // assert.equal(yates.power[2].start, 88)
        // assert.equal(yates.power[2].end, 96)
        // assert.equal(yates.power[2].value, "2B")


        // assert.equal(yates.name, "Kirby Yates")

        // assert.equal(yates.team, "SDP")


        await fs.writeFile(`../www/cards/2019-pitchers.csv`, JSON.stringify(cards))


    })



})
