
import assert = require('assert')

import moment = require('moment')

import { RollService } from '../src/service/roll-service'
import { HitterCardService } from '../src/service/hitter-card-service'

import { HitterDao } from '../src/dao/hitter-dao'
import { PitcherDao } from '../src/dao/pitcher-dao'
import { Hitter } from '../src/dto/hitter'
import { RollInput } from '../src/dto/roll-input'
import { RollChart } from '../src/dto/roll-chart'
import { Pitcher } from '../src/dto/pitcher'
import { HitterCard } from '../src/dto/hitter-card'
const fs = require('fs').promises;



//@ts-ignore
describe('HitterCardService', async (accounts) => {

    let service: HitterCardService
    let rollService:RollService
    let hitterDao = new HitterDao()
    let pitcherDao = new PitcherDao()

    //@ts-ignore
    before('Before', async () => {
        rollService = new RollService(hitterDao, pitcherDao)
        service = new HitterCardService(hitterDao, rollService)
    })

    //@ts-ignore
    it("should get a list of cards ", async () => {
        let cards:HitterCard[] = await service.list(2019)

        cards = cards.filter(card => card.team == "WSH" || card.team == "HOU")


        // assert.equal(cards.length, 451)

        //Check yelich
        // let yelich:HitterCard = cards[0]

        // assert.equal(yelich.leftContact[0].start, 18)
        // assert.equal(yelich.leftContact[0].end, 23)
        // assert.equal(yelich.leftContact[0].value, "O")

        // assert.equal(yelich.leftContact[1].start, 59)
        // assert.equal(yelich.leftContact[1].end, 67)
        // assert.equal(yelich.leftContact[1].value, "H")


        // assert.equal(yelich.leftPower[0].start, 31)
        // assert.equal(yelich.leftPower[0].end, 40)
        // assert.equal(yelich.leftPower[0].value, "BB")

        // assert.equal(yelich.leftPower[1].start, 71)
        // assert.equal(yelich.leftPower[1].end, 73)
        // assert.equal(yelich.leftPower[1].value, "2B")


        // assert.equal(yelich.leftPower[2].start, 83)
        // assert.equal(yelich.leftPower[2].end, 83)
        // assert.equal(yelich.leftPower[2].value, "3B")

        // assert.equal(yelich.leftPower[3].start, 84)
        // assert.equal(yelich.leftPower[3].end, 88)
        // assert.equal(yelich.leftPower[3].value, "HR")


        // assert.equal(yelich.rightContact[0].start, 18)
        // assert.equal(yelich.rightContact[0].end, 23)
        // assert.equal(yelich.rightContact[0].value, "O")

        // assert.equal(yelich.rightContact[1].start, 59)
        // assert.equal(yelich.rightContact[1].end, 67)
        // assert.equal(yelich.rightContact[1].value, "H")


        // assert.equal(yelich.rightPower[0].start, 31)
        // assert.equal(yelich.rightPower[0].end, 37)
        // assert.equal(yelich.rightPower[0].value, "BB")

        // assert.equal(yelich.rightPower[1].start, 71)
        // assert.equal(yelich.rightPower[1].end, 73)
        // assert.equal(yelich.rightPower[1].value, "2B")

        // assert.equal(yelich.rightPower[2].start, 81)
        // assert.equal(yelich.rightPower[2].end, 81)
        // assert.equal(yelich.rightPower[2].value, "3B")

        // assert.equal(yelich.rightPower[3].start, 82)
        // assert.equal(yelich.rightPower[3].end, 88)
        // assert.equal(yelich.rightPower[3].value, "HR")

        // assert.equal(yelich.name, "Christian Yelich")

        // assert.equal(yelich.running, 82)
        // assert.equal(yelich.team, "Brewers")


        await fs.writeFile(`../www/cards/2019-hitters.csv`, JSON.stringify(cards))


    })



})
