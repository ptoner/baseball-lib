
import assert = require('assert')
import moment = require('moment')

import { MlbApiService } from '../src/service/mlb-api-service'
import { MlbApiPlayer } from '../src/dto/mlb-api-player'

const fs = require('fs').promises

//@ts-ignore
describe('MlbApiService', async (accounts) => {

    let service: MlbApiService


    //@ts-ignore
    before('Before', async () => {
        service = new MlbApiService()
    })

    //@ts-ignore
    it("should get player", async () => {
        let trout:MlbApiPlayer = await service.findPlayer("Mike Trout", 2019, "Los Angeles Angels")
        
        assert.equal(trout.name_first, "Mike")
        assert.equal(trout.name_last,"Trout")
    })

    //@ts-ignore
    it("should get teams", async () => {
        let results = await service.getTeamsForSeason(2019)
        assert.equal(results.length, 49)
    })    


})
