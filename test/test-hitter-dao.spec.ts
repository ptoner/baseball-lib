
import assert = require('assert')

import moment = require('moment')
import { HitterDao} from "../src/dao/hitter-dao"
import { Hitter } from '../src/dto/hitter'


//@ts-ignore
describe('HitterDao', async (accounts) => {

    let dao: HitterDao


    //@ts-ignore
    before('Before', async () => {
        dao = new HitterDao()
    })    

    //@ts-ignore
    it("should load 2019", async () => {

        await dao.setYear(2019)

        let players:Hitter[] = dao.list()

        assert.equal(players.length, 451)

    })


})
