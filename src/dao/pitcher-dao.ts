import { Pitcher } from "../dto/pitcher"
let csvToJson = require('convert-csv-to-json');




class PitcherDao {

    records:Pitcher[]

    async setYear(year:number) {
        this.records = csvToJson.fieldDelimiter(',').formatValueByType().getJsonFromCsv(`../data/pitchers/${year}.csv`);
    }

    read(id:string):Pitcher {

        for (let pitcher of this.records) {
            if (pitcher.id == id) return pitcher
        }

    }

    list() : Pitcher[] {
        return this.records
    }

}

export { PitcherDao }