import { Hitter } from "../dto/hitter"
let csvToJson = require('convert-csv-to-json');




class HitterDao {

    records:Hitter[]

    async setYear(year:number) {
        this.records = csvToJson.fieldDelimiter(',').formatValueByType().getJsonFromCsv(`../data/hitters/${year}.csv`);
    }

    read(id:string):Hitter {

        for (let hitter of this.records) {
            if (hitter.id == id) return hitter
        }

    }

    list() : Hitter[] {
        return this.records
    }

}

export { HitterDao }