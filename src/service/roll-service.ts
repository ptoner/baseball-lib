import { RollInput } from "../dto/roll-input";
import { HitterDao } from "../dao/hitter-dao";
import { PitcherDao } from "../dao/pitcher-dao";
import { Hitter } from "../dto/hitter";
import { Pitcher } from "../dto/pitcher";
import { RollChart } from "../dto/roll-chart";

class RollService {

    constructor(
        private hitterDao: HitterDao,
        private pitcherDao: PitcherDao
    ) {}

    async getHitterLeagueAverages(year: number): Promise<RollInput> {

        await this.hitterDao.setYear(2019)

        let hitters: Hitter[] = this.hitterDao.list()

        let hitPercentSum = hitters.map(hitter => hitter.hit).reduce((a, b) => a + b)
        let kPercentSum = hitters.map(hitter => hitter.k).reduce((a, b) => a + b)

        let bbPercentSum = hitters.map(hitter => hitter.bb).reduce((a, b) => a + b)
        let singlePercentSum = hitters.map(hitter => hitter.single).reduce((a, b) => a + b)
        let doublePercentSum = hitters.map(hitter => hitter.double).reduce((a, b) => a + b)
        let triplePercentSum = hitters.map(hitter => hitter.triple).reduce((a, b) => a + b)
        let hrPercentSum = hitters.map(hitter => hitter.hr).reduce((a, b) => a + b)


        let hit = this.round(hitPercentSum / hitters.length) * 100
        let k = this.round(kPercentSum / hitters.length) * 100

        let bb = this.round(bbPercentSum / hitters.length) * 100
        let singles = this.round(singlePercentSum / hitters.length) * 100
        let doubles = this.round(doublePercentSum / hitters.length) * 100
        let triples = this.round(triplePercentSum / hitters.length) * 100
        let hr = this.round(hrPercentSum / hitters.length) * 100

        return this.buildRollInput(hit, k, bb, singles, doubles, triples, hr)


    }

    async getHitterYearRollInput(id: string, year: number, type:string): Promise<RollInput> {

        await this.hitterDao.setYear(year)

        let hitter: Hitter = this.hitterDao.read(id)

        switch(type) {

            case "L":
                return this.buildRollInput(
                    this.round(hitter.leftHit) * 100,
                    this.round(hitter.leftK) * 100,
                    this.round(hitter.leftBb) * 100,
                    this.round(hitter.leftSingle) * 100,
                    this.round(hitter.leftDouble) * 100,
                    this.round(hitter.leftTriple) * 100,
                    this.round(hitter.leftHr) * 100
                )

            case "R":
                return this.buildRollInput(
                    this.round(hitter.rightHit) * 100,
                    this.round(hitter.rightK) * 100,
                    this.round(hitter.rightBb) * 100,
                    this.round(hitter.rightSingle) * 100,
                    this.round(hitter.rightDouble) * 100,
                    this.round(hitter.rightTriple) * 100,
                    this.round(hitter.rightHr) * 100
                )


        }

    }

    async getPitcherYearRollInput(id: string, year: number, type:string): Promise<RollInput> {

        await this.pitcherDao.setYear(year)

        let pitcher: Pitcher = this.pitcherDao.read(id)

        switch(type) {
            case "L":
                return this.buildRollInput(
                    this.round(pitcher.leftHit) * 100,
                    this.round(pitcher.leftK) * 100,
                    this.round(pitcher.leftBb) * 100,
                    this.round(pitcher.leftSingle) * 100,
                    this.round(pitcher.leftDouble) * 100,
                    this.round(pitcher.leftTriple) * 100,
                    this.round(pitcher.leftHr) * 100
                )

            case "R":
                return this.buildRollInput(
                    this.round(pitcher.rightHit) * 100,
                    this.round(pitcher.rightK) * 100,
                    this.round(pitcher.rightBb) * 100,
                    this.round(pitcher.rightSingle) * 100,
                    this.round(pitcher.rightDouble) * 100,
                    this.round(pitcher.rightTriple) * 100,
                    this.round(pitcher.rightHr) * 100
                )


        }

    }

    getContactRollChart(input: RollInput): RollChart {

        let chart: RollChart = {}
        chart.entries = new Map<number, string>()

        let kCount = 0
        let sfCount = 0
        let outCount = 0

        for (let i = 0; i < 100; i++) {

            if (kCount < input.k) {
                chart.entries.set(i, "K")
                kCount++
                continue
            }

            if (outCount < input.out) {
                chart.entries.set(i, "O")
                outCount++
                continue
            }

            chart.entries.set(i, "H")

        }

        return chart
    }

    getPowerRollChart(input: RollInput): RollChart {

        let chart: RollChart = {}
        chart.entries = new Map<number, string>()

        let bbCount = 0
        let singleCount = 0
        let doubleCount = 0
        let tripleCount = 0

        for (let i = 0; i < 100; i++) {

            if (bbCount < input.bb) {
                chart.entries.set(i, "BB")
                bbCount++
                continue
            }

            if (singleCount < input.singles) {
                chart.entries.set(i, "1B")
                singleCount++
                continue
            }

            if (doubleCount < input.doubles) {
                chart.entries.set(i, "2B")
                doubleCount++
                continue
            }

            if (tripleCount < input.triples) {
                chart.entries.set(i, "3B")
                tripleCount++
                continue
            }

            chart.entries.set(i, "HR")

        }

        return chart

    }

    diffRollChart(average: RollChart, override: RollChart): RollChart {

        let result: RollChart = {}
        result.entries = new Map<number, string>()

        for (let i = 0; i < 100; i++) {
            if (override.entries.get(i) != average.entries.get(i)) {
                result.entries.set(i, override.entries.get(i))
            }
        }

        return result

    }

    async simSinglePlayer(id: number, year: number, iterations: number): Promise<RollInput> {

        let rollInput: RollInput = await this.getHitterYearRollInput(id.toString(), year, "L")

        let contactChart: RollChart = this.getContactRollChart(rollInput)
        let powerChart: RollChart = this.getPowerRollChart(rollInput)

        return this.simCharts(contactChart, powerChart, iterations)
    }

    async simCharts(contactChart: RollChart, powerChart: RollChart, iterations: number) {

        let kCount = 0
        let oCount = 0
        let sfCount = 0
        let bbCount = 0
        let singleCount = 0
        let doubleCount = 0
        let tripleCount = 0
        let hrCount = 0
        let hitCount = 0
        let total = 0


        for (let i = 0; i < iterations; i++) {

            let result

            let die1 = Math.floor(Math.random() * (99 + 1))
            let contactResult = contactChart.entries.get(die1)

            if (contactResult == "H") {
                let die2 = Math.floor(Math.random() * (99 + 1))
                result = powerChart.entries.get(die2)
            } else {
                result = contactResult
            }

            switch (result) {
                case "O":
                    oCount++
                    break
                case "K":
                    kCount++
                    break
                case "SF":
                    sfCount++
                    break
                case "BB":
                    bbCount++
                    hitCount++
                    break
                case "1B":
                    singleCount++
                    hitCount++
                    break
                case "2B":
                    doubleCount++
                    hitCount++
                    break
                case "3B":
                    tripleCount++
                    hitCount++
                    break
                case "HR":
                    hrCount++
                    hitCount++
                    break
            }

            total++

        }

        return {
            hit: hitCount / total,
            out: oCount / total,
            k: kCount / total,
            sf: sfCount / total,

            bb: bbCount / (total - kCount - sfCount - oCount),
            singles: singleCount / (total - kCount - sfCount - oCount),
            doubles: doubleCount / (total - kCount - sfCount - oCount),
            triples: tripleCount / (total - kCount - sfCount - oCount),
            hr: hrCount / (total - kCount - sfCount - oCount)
        }
    }

    async simMatchup(hitterId: string, pitcherId: string, year: number, iterations: number): Promise<RollInput> {

        let leagueAvgRollInput: RollInput = await this.getHitterLeagueAverages(year)

        let leagueAvgContactChart: RollChart = this.getContactRollChart(leagueAvgRollInput)
        let leagueAvgPowerChart: RollChart = this.getPowerRollChart(leagueAvgRollInput)


        //Hitter
        let hitterRollInput: RollInput = await this.getHitterYearRollInput(hitterId, year, "L")

        let hitterContactDiffChart:RollChart = await this.getContactDiff(leagueAvgContactChart, hitterRollInput)
        let hitterPowerDiffChart:RollChart = await this.getPowerDiff(leagueAvgPowerChart, hitterRollInput)


        //Pitcher
        let pitcherRollInput: RollInput = await this.getPitcherYearRollInput(pitcherId, year,"L")
        
        let pitcherContactDiffChart:RollChart = await this.getContactDiff(leagueAvgContactChart, pitcherRollInput)
        let pitcherPowerDiffChart:RollChart = await this.getPowerDiff(leagueAvgPowerChart, pitcherRollInput)



        let contactChart: RollChart = this.applyChartDiffs(hitterContactDiffChart, pitcherContactDiffChart, leagueAvgContactChart)
        let powerChart: RollChart = this.applyChartDiffs(hitterPowerDiffChart, pitcherPowerDiffChart, leagueAvgPowerChart)

        return this.simCharts(contactChart, powerChart, iterations)

    }


    async getContactDiff(average:RollChart, input:RollInput) : Promise<RollChart> {
        let rollChart: RollChart = this.getContactRollChart(input)
        return this.diffRollChart(average, rollChart)
    }

    async getPowerDiff(average:RollChart, input:RollInput) : Promise<RollChart> {
        let rollChart: RollChart = this.getPowerRollChart(input)
        return this.diffRollChart(average, rollChart)
    }

    private buildRollInput(hit: number, k: number, bb: number, singles: number, doubles: number, triples: number, hr: number): RollInput {

        let result: RollInput = {}
        result.hit = this.round(hit)
        result.k = this.round(k)
        result.out = 100 - result.k - result.hit

        result.bb = this.round(bb)
        result.singles = this.round(bb + singles) - result.bb
        result.doubles = this.round(bb + singles + doubles) - result.bb - result.singles
        result.triples = this.round(bb + singles + doubles + triples) - result.bb - result.singles - result.doubles
        result.hr = 100 - result.bb - result.singles - result.doubles - result.triples

        return result
    }

    public applyChartDiffs(diff1: RollChart, diff2: RollChart, average: RollChart): RollChart {

        for (let i=0; i < 100; i++) {

            let chart1Value = diff1.entries.get(i)
            let chart2Value = diff2.entries.get(i)

            if (chart1Value && !chart2Value) {
                average.entries.set(i, chart1Value)
            }

            if (chart2Value && !chart1Value) {
                average.entries.set(i, chart2Value)
            }
        }

        return average

    }

    public round(number) {
        let result = Math.round(number * 100) / 100
        return result
    }

}

export { RollService }