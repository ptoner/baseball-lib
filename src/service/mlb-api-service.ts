import { MlbApiPlayer } from "../dto/mlb-api-player"

const fetch = require('node-fetch')

const sha256 = require('js-sha256')
const fs = require('fs').promises;


class MlbApiService {

    async findPlayer(playerName:string, season, teamName) : Promise<MlbApiPlayer> {

        let player = await this.lookupPlayer(playerName, season,teamName, true)

        if (!player) {
            //Try in inactive players
            player = await this.lookupPlayer(playerName, season,teamName, false)
        }


        if (!player) {
            //Look up by last name
            let names = playerName.split(" ")
            player = await this.lookupPlayer(names[1], season,teamName, true)


            if (!player) {
                //Try in inactive players
                player = await this.lookupPlayer(names[1], season,teamName, false)
            }

        }



        if (player) {
            let filename = sha256(`${playerName}-${season}-${teamName}`)
            await fs.writeFile(`../data/raw/players/${filename}`, JSON.stringify(player))
        }

        return player
    }


    async lookupPlayer(playerName, season, teamName, active:boolean) : Promise<MlbApiPlayer> {

        //See if we already have the file for this query
        let filename = sha256(`${playerName}-${season}-${teamName}`)

        let existingFile
        try {
            existingFile = await fs.readFile(`../data/raw/players/${filename}`)
            let existingPlayer = JSON.parse(existingFile.toString())
            console.log(`Found player: ${existingPlayer.name_display_first_last} - ${existingPlayer.team_abbrev}`)
            return existingPlayer
        } catch(ex) {
            console.log(ex)
        }


        let url = `https://lookup-service-prod.mlb.com/json/named.search_player_all.bam?sport_code=%27mlb%27&active_sw=%27${active ? "Y" : "N"}%27&name_part=%27${playerName}%25%27`
    
        let result = await fetch(url)
        let parsed = JSON.parse(await result.text())
    
        //Could have multiple results. If there's a single result it puts it in an object instead of an array. Fix that.
        let playerMatches = parsed.search_player_all.queryResults.row
    
        if (!this.isIterable(playerMatches)) {
            playerMatches = [playerMatches]
        }
    
        let matchingPlayer 
        for (let playerMatch of playerMatches) {

            if (!playerMatch) continue

            if (teamName) {
                let teams = await this.getTeamsForPlayerAndSeason(playerMatch.player_id, season)
    
                for (let team of teams) {
                    if (team && team.team == teamName) {
                        matchingPlayer = playerMatch
                    }
                }
            } else {
                matchingPlayer = playerMatch 
            }


        }





        return matchingPlayer
    }


    async getTeamsForSeason(season:number)  {

        let url = `https://lookup-service-prod.mlb.com/json/named.team_all_season.bam?sport_code=%27mlb%27&season=${season}`
    
        let result = await fetch(url)
        let parsed = JSON.parse(await result.text())
    
        //Could have multiple results. If there's a single result it puts it in an object instead of an array. Fix that.
        let matches = parsed.team_all_season.queryResults.row
    
        if (!this.isIterable(matches)) {
            matches = [matches]
        }
    
        return matches 

    }


    async getTeamsForPlayerAndSeason(playerId, season) {

        let url = `https://lookup-service-prod.mlb.com/json/named.player_teams.bam?season=${season}&player_id=${playerId}`
        let result = await fetch(url)
        let parsed = JSON.parse(await result.text())
    
        let results = parsed.player_teams.queryResults.row
    
        if (!this.isIterable(results)) {
            results = [results]
        }
    
        return results
    }

    isIterable(obj) {
        // checks for null and undefined
        if (obj == null) {
            return false;
        }
        return typeof obj[Symbol.iterator] === 'function';
    }
}

export {
    MlbApiService
}