import { Hitter } from "../dto/hitter"
import { transcode } from "buffer"
import { MlbApiPlayer } from "../dto/mlb-api-player";
import { MlbApiService } from "./mlb-api-service";
import { AssertionError } from "assert";
let csvToJson = require('convert-csv-to-json')
const { Parser } = require('json2csv');
const fs = require('fs').promises;
import assert = require('assert')




class RawHitterService {

    full:RawHitterRow[]
    left:RawHitterRow[]
    right:RawHitterRow[]

    constructor(
        private mlbApiService:MlbApiService
    ){}

    async setYear(year:number) {
        this.full = csvToJson.fieldDelimiter(',').formatValueByType().getJsonFromCsv(`../data/raw/hitters/${year}-full.csv`)
        this.left = csvToJson.fieldDelimiter(',').formatValueByType().getJsonFromCsv(`../data/raw/hitters/${year}-L.csv`)
        this.right = csvToJson.fieldDelimiter(',').formatValueByType().getJsonFromCsv(`../data/raw/hitters/${year}-R.csv`)
    }


    async processHitters(year:number) {

        await this.setYear(year)

        let results:Hitter[] = []

        let filtered:RawHitterRow[] = this.full.filter(row => row.PA > 100)

        this.calculateRunning(filtered)

        let teams = await this.mlbApiService.getTeamsForSeason(year)


        for (let full of filtered) {

            let translated:Hitter = this.translateRawHitterRow(full)
            translated.running = full.bsr


            //Look up team
            let team = teams.filter(theTeam => theTeam.name == full.team || theTeam.name_display_long.includes(full.team)  || theTeam.name_abbrev == full.team)[0]

            //Look up player
            let mlbPlayer = await this.mlbApiService.findPlayer(full.name, year, team ? team.name_display_long : undefined)

            //Some players we can't get the team until we know which player. Weird.
            if (!team) {
                let teams = await this.mlbApiService.getTeamsForPlayerAndSeason(mlbPlayer.player_id, year)
                team = teams[teams.length-1]

                assert.equal(teams.length < 49, true) //If we got all the teams something is screwed

            }

            assert.notEqual(team,undefined)
            assert.notEqual(mlbPlayer, undefined)

            translated.bats = mlbPlayer.bats
            translated.id = mlbPlayer.player_id

            translated.name = mlbPlayer.name_display_first_last
            translated.team = team.mlb_org_abbrev
            translated.position = mlbPlayer.position

            //Left 
            let left:RawHitterRow = this.getLeft(full.id)

            if (left) {
                let leftTranslated:Hitter = this.translateRawHitterRow(left)

                translated.leftHit = leftTranslated.hit ? leftTranslated.hit : 0
                translated.leftK = leftTranslated.k ? leftTranslated.k : 0
                translated.leftOut = leftTranslated.out ? leftTranslated.out : 0
                translated.leftSf = leftTranslated.sf ? leftTranslated.sf : 0
    
                translated.leftBb = leftTranslated.bb ? leftTranslated.bb : 0
                translated.leftSingle = leftTranslated.single ? leftTranslated.single : 0
                translated.leftDouble = leftTranslated.double ? leftTranslated.double : 0
                translated.leftTriple = leftTranslated.triple ? leftTranslated.triple : 0
                translated.leftHr = leftTranslated.hr ? leftTranslated.hr : 0
    
            }


            //Right
            let right:RawHitterRow = this.getRight(full.id)

            if (right) {
                let rightTranslated:Hitter = this.translateRawHitterRow(right)
                
                translated.rightHit = rightTranslated.hit ? rightTranslated.hit : 0
                translated.rightK = rightTranslated.k ? rightTranslated.k : 0
                translated.rightOut = rightTranslated.out ? rightTranslated.out : 0
                translated.rightSf = rightTranslated.sf ? rightTranslated.sf : 0
    
                translated.rightBb = rightTranslated.bb ? rightTranslated.bb : 0
                translated.rightSingle = rightTranslated.single ? rightTranslated.single : 0
                translated.rightDouble = rightTranslated.double ? rightTranslated.double : 0
                translated.rightTriple = rightTranslated.triple ? rightTranslated.triple : 0
                translated.rightHr = rightTranslated.hr ? rightTranslated.hr : 0
            }


            results.push(translated)
        }


        const fields = ["name","id","team", "position","bats","hit","sf","k","out","bb","single","double","triple","hr","leftHit","leftK","leftOut","leftSf","leftBb","leftSingle","leftDouble","leftTriple","leftHr","rightHit","rightK","rightOut","rightSf","rightBb","rightSingle","rightDouble","rightTriple","rightHr", "running"];
        const json2csvParser = new Parser({ fields, quote: '' })
        const csv = json2csvParser.parse(results)

        await fs.writeFile(`../data/hitters/${year}.csv`, csv)

    }

    private translateRawHitterRow(hitter: RawHitterRow) : Hitter {

        let translated: Hitter = {}


        let hitCount = (hitter.singles + hitter.doubles + hitter.triples + hitter.HR + hitter.BB + hitter.IBB + hitter.HBP)

        translated.hit = hitCount ? hitCount / hitter.PA : 0
        translated.sf = hitter.SF ? hitter.SF / hitter.PA : 0
        translated.k = hitter.SO ? hitter.SO / hitter.PA : 0
        translated.out = 1 - translated.hit - translated.sf - translated.k

        translated.bb =     hitCount ? (hitter.BB + hitter.IBB + hitter.HBP) / hitCount : 0
        translated.single = hitCount ? hitter.singles / hitCount : 0
        translated.double = hitCount ? hitter.doubles / hitCount : 0
        translated.triple = hitCount ? hitter.triples / hitCount : 0
        translated.hr =     hitCount ? hitter.HR / hitCount : 0

        return translated 
    }

    calculateRunning(rows:RawHitterRow[]) {


        //Get min/max
        let min:number = Math.min.apply(null, rows.map(row => row.bsr)) 
        let max:number = Math.max.apply(null, rows.map(row => row.bsr))


        //Normalize from 45-86
        let newMin = 45
        let newMax = 87

        //Normalize all of the values 
        for (let i = 0; i < rows.length; i++) {

            let value = rows[i].bsr

            value = Math.round(((value - min) / (max - min)) * 100)

            //Normalize from 45-87
            rows[i].bsr = Math.round(( (value - 0) / (100 - 0) ) * (newMax - newMin) + newMin)

        }

    }


    getLeft(id:number) : RawHitterRow {
        for (let hitter of this.left) {
            if (hitter.id == id) return hitter
        }
    }

    getRight(id:number) : RawHitterRow {
        for (let hitter of this.right) {
            if (hitter.id == id) return hitter
        }
    }


}

interface RawHitterRow {
    name:string 
    team:string
    PA:number
    singles:number
    doubles:number
    triples:number
    HR:number
    BB:number
    IBB:number
    SO:number
    HBP:number
    SF:number
    SH:number
    SB:number
    bsr:number
    def:number
    id:number
}


export { RawHitterService }