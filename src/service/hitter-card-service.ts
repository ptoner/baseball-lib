
import { HitterCard } from "../dto/hitter-card";
import { HitterDao } from "../dao/hitter-dao";
import { Hitter } from "../dto/hitter";
import { RollService } from "./roll-service";
import { RollInput } from "../dto/roll-input";
import { RollChart } from "../dto/roll-chart";
import { ChartOverride } from "../dto/chart-override";

class HitterCardService {


    constructor(
        private hitterDao:HitterDao,
        private rollService:RollService
    ) {}

    async list(year:number) : Promise<HitterCard[]> {
        
        const result:HitterCard[] = []

        this.hitterDao.setYear(year)

        //Get league average
        let averages:RollInput = await this.rollService.getHitterLeagueAverages(year)
        let averageContactChart:RollChart = this.rollService.getContactRollChart(averages)
        let averagePowerChart:RollChart = this.rollService.getPowerRollChart(averages)



        let hitters:Hitter[] = this.hitterDao.list()

        for (let hitter of hitters)  {

            const card:HitterCard = {}

            card.running = hitter.running

            card.name = hitter.name
            card.team = hitter.team
            card.bats = hitter.bats
            card.position = hitter.position

            //Left chart
            let leftInput:RollInput = await this.rollService.getHitterYearRollInput(hitter.id, year, "L")
            const leftContact:RollChart = await this.rollService.getContactDiff(averageContactChart, leftInput)
            const leftPower:RollChart = await this.rollService.getPowerDiff(averagePowerChart, leftInput)

            //Right chart
            let rightInput:RollInput = await this.rollService.getHitterYearRollInput(hitter.id, year, "R")
            const rightContact:RollChart = await this.rollService.getContactDiff(averageContactChart, leftInput)
            const rightPower:RollChart = await this.rollService.getPowerDiff(averagePowerChart, rightInput)


            //Generate list of ChartOverrides
            card.leftContact = this.getOverrides(leftContact)
            card.leftPower = this.getOverrides(leftPower)

            card.rightContact = this.getOverrides(rightContact)
            card.rightPower = this.getOverrides(rightPower)

            result.push(card)
        }


        return result

    }


    getOverrides(chart:RollChart) : ChartOverride[] {
            
        let overrides:ChartOverride[] = []

        let entries = Array.from(chart.entries.entries())

        for (let entry of entries) {
            let key = entry[0]
            let value = entry[1]

            //Check in overrides for previous number.
            let match:ChartOverride = this.match(key,value, overrides)

            if (match) {
                match.end = key 
            } else {
                overrides.push({
                    start: key,
                    end: key,
                    value: value 
                })
            }
        }

        return overrides

    }

    match(key:number, value:string, overrides:ChartOverride[]) : ChartOverride {
        let previous = key-1 
        let matches:ChartOverride[] = overrides.filter(override => (override.start == previous || override.end == previous) && override.value == value)
        
        if (matches && matches.length > 0) {
            return matches[0]
        }
    }


} 

export { HitterCardService }