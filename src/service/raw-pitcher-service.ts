import { Pitcher } from "../dto/pitcher"
import { transcode } from "buffer"
import { MlbApiService } from "./mlb-api-service"
let csvToJson = require('convert-csv-to-json')
const { Parser } = require('json2csv')
const fs = require('fs').promises
import assert = require('assert')




class RawPitcherService {

    full:RawPitcherRow[]
    
    constructor(
        private mlbApiService:MlbApiService
    ){}

    async setYear(year:number) {
        this.full = csvToJson.fieldDelimiter(',').formatValueByType().getJsonFromCsv(`../data/raw/pitchers/${year}.csv`)
    }


    async processPitchers(year:number) {

        await this.setYear(year)

        let results:Pitcher[] = []

        let teams = await this.mlbApiService.getTeamsForSeason(year)


        //There's multiple rows for each pitcher. Group them by player/team so we can get a list of unique ones. 
        let players= []
        this.full.forEach(row => {
            let existing = players.filter(player => player.name == row.name && player.team == row.team)

            if (!existing || existing.length == 0) {
                players.push({
                    name: row.name,
                    team: row.team
                })
            }
        })

        let id=0

        for (let player of players) {

            id++

            let translated:Pitcher = {}
            
            
            //Look up team
            let team = teams.filter(theTeam => theTeam.name == player.team || theTeam.name_display_long.includes(player.team) || theTeam.name_abbrev == player.team)[0]

            //Look up player
            let mlbPlayer = await this.mlbApiService.findPlayer(player.name, year, team ? team.name_display_long : undefined)

            //Some players we can't get the team until we know which player. Weird.
            if (!team) {
                let teams = await this.mlbApiService.getTeamsForPlayerAndSeason(mlbPlayer.player_id, year)
                team = teams[teams.length-1]

                assert.equal(teams.length < 49, true) //If we got all the teams something is screwed
            }

            if (!team) {
                console.log(`Skipping ${player.name}, no team found`)
                continue
            }

            if (mlbPlayer.position != "P") {
                console.log(`Skipping ${player.name}. Not a pitcher`)
                continue
            }


            assert.notEqual(team,undefined)
            assert.notEqual(mlbPlayer, undefined)

            translated.throws = mlbPlayer.bats
            
            translated.id = mlbPlayer.player_id
            translated.name = mlbPlayer.name_display_first_last
            translated.team = team.org_abbrev

            let left:RawPitcherRow = this.getLeft(translated.name)

            if (left) {
                let leftTranslated:Pitcher = this.translateRawPitcherRow(left)

                translated.leftPa = leftTranslated.pa ? leftTranslated.pa : 0
                translated.leftHit = leftTranslated.hit ? leftTranslated.hit : 0
                translated.leftK = leftTranslated.k ? leftTranslated.k : 0
                translated.leftOut = leftTranslated.out ? leftTranslated.out : 0
                translated.leftSf = leftTranslated.sf ? leftTranslated.sf : 0
    
                translated.leftBb = leftTranslated.bb ? leftTranslated.bb : 0
                translated.leftSingle = leftTranslated.single ? leftTranslated.single : 0
                translated.leftDouble = leftTranslated.double ? leftTranslated.double : 0
                translated.leftTriple = leftTranslated.triple ? leftTranslated.triple : 0
                translated.leftHr = leftTranslated.hr ? leftTranslated.hr : 0
                
            }

            //Right
            let right:RawPitcherRow = this.getRight(translated.name)

            if (right) {
                let rightTranslated:Pitcher = this.translateRawPitcherRow(right)
                
                translated.rightPa = rightTranslated.pa ? rightTranslated.pa : 0
                translated.rightHit = rightTranslated.hit ? rightTranslated.hit : 0
                translated.rightK = rightTranslated.k ? rightTranslated.k : 0
                translated.rightOut = rightTranslated.out ? rightTranslated.out : 0
                translated.rightSf = rightTranslated.sf ? rightTranslated.sf : 0
    
                translated.rightBb = rightTranslated.bb ? rightTranslated.bb : 0
                translated.rightSingle = rightTranslated.single ? rightTranslated.single : 0
                translated.rightDouble = rightTranslated.double ? rightTranslated.double : 0
                translated.rightTriple = rightTranslated.triple ? rightTranslated.triple : 0
                translated.rightHr = rightTranslated.hr ? rightTranslated.hr : 0
            }

            translated.pa = translated.leftPa + translated.rightPa


            let bfPerG = translated.pa / player.G

            if (bfPerG > 10) {
                translated.pitches = 4
            } else if(bfPerG > 5) {
                translated.pitches = 3
            } else {
                translated.pitches = 2
            }
    


            results.push(translated)
        }

        let filtered:Pitcher[] = results.filter(row => row.pa > 75)



        const fields = ["name","id","team","throws","leftHit","leftK","leftOut","leftSf","leftBb","leftSingle","leftDouble","leftTriple","leftHr","rightHit","rightK","rightOut","rightSf","rightBb","rightSingle","rightDouble","rightTriple","rightHr", "pitches"];
        const json2csvParser = new Parser({ fields, quote: '' })
        const csv = json2csvParser.parse(filtered)

        await fs.writeFile(`../data/pitchers/${year}.csv`, csv)

    }

    private translateRawPitcherRow(pitcher: RawPitcherRow) : Pitcher {

        let translated: Pitcher = {}

        let hitCount = (pitcher.singles + pitcher.doubles + pitcher.triples + pitcher.HR + pitcher.BB + pitcher.IBB + pitcher.HBP)

        translated.pa = pitcher.PA
        translated.hit = hitCount ? hitCount / pitcher.PA : 0
        translated.sf = pitcher.SF ? pitcher.SF / pitcher.PA : 0
        translated.k = pitcher.SO ? pitcher.SO / pitcher.PA : 0
        translated.out = 1 - translated.hit - translated.sf - translated.k

        translated.bb =     hitCount ? (pitcher.BB + pitcher.IBB + pitcher.HBP) / hitCount : 0
        translated.single = hitCount ? pitcher.singles / hitCount : 0
        translated.double = hitCount ? pitcher.doubles / hitCount : 0
        translated.triple = hitCount ? pitcher.triples / hitCount : 0
        translated.hr =     hitCount ? pitcher.HR / hitCount : 0

        return translated 
    }


    getLeft(name:string) : RawPitcherRow {

        let matches = this.full.filter(pitcher => pitcher.name == name &&  pitcher.split == "vs LHB")

        if (matches && matches.length > 1) {
            throw new Error("More than one match")
        }

        if (matches && matches.length > 0) {
            return matches[0]
        }

    }

    getRight(name:string) : RawPitcherRow {

        let matches = this.full.filter(pitcher => pitcher.name == name &&  pitcher.split == "vs RHB")

        if (matches && matches.length > 1) {
            throw new Error("More than one match")
        }

        if (matches && matches.length > 0) {
            return matches[0]
        }
    }


}

interface RawPitcherRow {
    name:string 
    split:string 
    team:string
    year:number 
    PA:number
    G:number 
    H:number 
    singles:number
    doubles:number
    triples:number
    HR:number
    BB:number
    IBB:number
    SO:number
    HBP:number
    SF:number
    SH:number

}


export { RawPitcherService }