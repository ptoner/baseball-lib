import { ChartOverride } from "./chart-override"
import { RollChart } from "./roll-chart"

class PitcherCard {

    name?:string 
    team?:string 
    throws?:string

    pitches?:number


    leftContact?:ChartOverride[] 
    leftPower?:ChartOverride[]

    rightContact?:ChartOverride[] 
    rightPower?:ChartOverride[]
}

export { PitcherCard }