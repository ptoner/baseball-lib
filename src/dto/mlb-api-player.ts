interface MlbApiPlayer {

    active_sw:string
    bats:string
    birth_city:string
    birth_country:string
    birth_date:string
    birth_state:string
    college:string
    height_feet:string
    height_inches:string
    high_school:string
    league:string
    name_display_first_last:string
    name_display_last_first:string
    name_display_roster:string
    name_first:string
    name_last:string
    name_use:string
    player_id:string
    position:string
}

export { MlbApiPlayer}