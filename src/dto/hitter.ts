
interface Hitter {

    id?:string
    name?:string
    year?:number
    team?:string
    position?:string
    bats?:string

    //All
    hit?:number
    k?:number
    out?:number
    sf?:number

    bb?:number
    single?:number
    double?:number
    triple?:number
    hr?:number


    //Left 
    leftHit?:number
    leftK?:number
    leftOut?:number
    leftSf?:number

    leftBb?:number
    leftSingle?:number
    leftDouble?:number
    leftTriple?:number
    leftHr?:number

    //Right
    rightHit?:number
    rightK?:number
    rightOut?:number
    rightSf?:number

    rightBb?:number
    rightSingle?:number
    rightDouble?:number
    rightTriple?:number
    rightHr?:number

    running?:number 

}

export { Hitter }