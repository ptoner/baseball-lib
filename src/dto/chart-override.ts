interface ChartOverride {
    start:number
    end:number 
    value:string 
}

export {
    ChartOverride
}