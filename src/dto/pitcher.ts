interface Pitcher {

    id?: string
    name?: string
    year?: number
    team?: string
    throws?:string 

    //All
    pa?:number
    hit?:number
    k?:number
    out?:number
    sf?:number

    bb?:number
    single?:number
    double?:number
    triple?:number
    hr?:number

    //Left 
    leftPa?:number 
    leftHit?: number
    leftK?: number
    leftOut?: number
    leftSf?: number

    leftBb?: number
    leftSingle?: number
    leftDouble?: number
    leftTriple?: number
    leftHr?: number

    //Right
    rightPa?:number 
    rightHit?: number
    rightK?: number
    rightOut?: number
    rightSf?: number

    rightBb?: number
    rightSingle?: number
    rightDouble?: number
    rightTriple?: number
    rightHr?: number

    pitches?: number

}

export { Pitcher }