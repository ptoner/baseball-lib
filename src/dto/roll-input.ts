
interface RollInput {

    hit?: number 
    k?: number 
    out?: number 

    bb?: number 
    singles?: number 
    doubles?: number 
    triples?: number 
    hr?: number 
}

export { RollInput }