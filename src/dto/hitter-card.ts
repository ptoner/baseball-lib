import { ChartOverride } from "./chart-override"
import { RollChart } from "./roll-chart"

class HitterCard {

    name?:string 
    team?:string 
    position?:string 
    bats?:string 

    leftContact?:ChartOverride[] 
    leftPower?:ChartOverride[]

    rightContact?:ChartOverride[] 
    rightPower?:ChartOverride[]

    running?:number 

}

export { HitterCard }