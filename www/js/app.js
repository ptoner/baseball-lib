
async function run() {

    let hitterResponse = await fetch("../cards/2019-hitters.csv")
    let hitters = await hitterResponse.json();



    let pitcherResponse = await fetch("../cards/2019-pitchers.csv")
    let pitchers = await pitcherResponse.json();

    
    var compiledHitter = Template7.compile($('#hitterTemplate').html())
    var compiledPitcher = Template7.compile($('#pitcherTemplate').html());
    
    
    let hitterCount=0
    for (let hitter of hitters) {
        
        $('#mainRow').append($(compiledHitter(hitter)))
        hitterCount++ 

        if (hitterCount >= 16) {
            $('#mainRow').append($('<div class="pagebreak"></div>'))
            hitterCount=0
        }
    }
    
    let pitcherCount=0
    for (let pitcher of pitchers) {
    
        $('#mainRow').append($(compiledPitcher(pitcher)))

        pitcherCount++ 

        if (pitcherCount >= 16) {
            $('#mainRow').append($('<div class="pagebreak"></div>'))
            pitcherCount=0
        }

    }
    
}


run()